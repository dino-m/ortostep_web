<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @inheritdoc
 */
class CommonAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $basePath = '@web';
    /**
     * @inheritdoc
     */
    public $baseUrl = '@web';
    /**
     * @inheritdoc
     */
    public $depends = [
//        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
    /**
     * @inheritdoc
     */
    public $js = [
        'https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js',
        'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js',
        'https://use.fontawesome.com/releases/v5.0.8/js/solid.js',
        'https://use.fontawesome.com/releases/v5.0.8/js/fontawesome.js',
        //'https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js',
        //'https://cdnjs.cloudflare.com/ajax/libs/flickity/2.0.9/flickity.pkgd.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js',
        'https://unpkg.com/flickity-fullscreen@1/fullscreen.js',
        'js/scrollfix.js',
        'js/flickity.pkgd.min.js',
        'js/site.js'
    ];
    /**
     * @inheritdoc
     */
    public $css = [
        'https://fonts.googleapis.com/css?family=Open+Sans:400,700|Raleway:400,600,800&amp;subset=latin-ext',
        'https://fonts.googleapis.com/css?family=Oswald|Roboto:400,500&amp;subset=latin-ext',
        'https://fonts.googleapis.com/css?family=Courgette|Fresca',
        'https://fonts.googleapis.com/css?family=Comfortaa|Raleway',
        'https://fonts.googleapis.com/css?family=Comfortaa|Dancing+Script|Shadows+Into+Light',
        'https://unpkg.com/flickity@2/dist/flickity.min.css',
        'https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css',
        'https://use.fontawesome.com/releases/v5.0.8/css/all.css',
        'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',
        'https://unpkg.com/flickity-fullscreen@1/fullscreen.css',
        'css/site.css',
        'css/common.css',
        'css/flickity.css',
        
    ];
}
