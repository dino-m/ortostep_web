<?php

define('APP_INSTALLATION_FILE', dirname(__DIR__) . DS . 'commands' . DS . '.installed');
define('APP_TIMEZONE', 'Europe/Zagreb'); //http://php.net/manual/en/timezones.php
define('APP_NAME', 'ortostep.hr');
define('APP_SLUG', 'ortostep-hr');
define('APP_VERSION', '1.0.0');

define('APP_PROTOCOL', isset($_SERVER['HTTPS']) ? 'https' : 'http');
define('APP_HOST', isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '');
define('APP_RELATIVE_BASE_URL', '');
define('APP_ABSOLUTE_BASE_URL', APP_PROTOCOL . '://' . APP_HOST . APP_RELATIVE_BASE_URL);
define('APP_EMAIL_FROM_NO_REPLY', 'no-reply@ortostep.com');
define('APP_EMAIL_ADMIN', '******');

define('APP_DATABASE_USERNAME', '******');
define('APP_DATABASE_PASSWORD', '******');
define('APP_DATABASE_HOST', '127.0.0.1');
define('APP_DATABASE_PORT', 3306);
define('APP_DATABASE_SCHEMA', '******');
define('APP_DATABASE_CHARACTER_SET', 'utf8mb4');
define('APP_DATABASE_COLLATE', 'utf8_general_ci');

Yii::setAlias('@root', dirname(__DIR__));
Yii::setAlias('@models', dirname(__DIR__) . DS);
Yii::setAlias('@controllers', dirname(__DIR__) . DS . 'controllers');
Yii::setAlias('@views', dirname(__DIR__) . DS . 'views');
Yii::setAlias('@tests', dirname(__DIR__) . DS . 'tests');
Yii::setAlias('@console', dirname(__DIR__) . DS . 'commands');
Yii::setAlias('@migrations', dirname(__DIR__) . DS . 'migrations');
Yii::setAlias('@web', dirname(__DIR__) . DS . 'web');
Yii::setAlias('@webroot', dirname(__DIR__) . DS . 'web');
Yii::setAlias('@runtime', dirname(__DIR__) . DS . 'runtime');
Yii::setAlias('@pathCategoryImage', dirname(__DIR__) . DS . 'web'. DS . 'img' . DS . 'products');
Yii::setAlias('@urlCategoryImage', APP_ABSOLUTE_BASE_URL.'/../web/img/products');
Yii::setAlias('@urlRootImage', APP_ABSOLUTE_BASE_URL.'/../web/img');
Yii::setAlias('@urlBannerImage', APP_ABSOLUTE_BASE_URL.'/../web/img/banners');
Yii::setAlias('@urlDownloadImage', APP_ABSOLUTE_BASE_URL.'/../web/download');
Yii::setAlias('@urlCategoryLink', APP_ABSOLUTE_BASE_URL.'/../web/category');
