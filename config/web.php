<?php

use yii\helpers\ArrayHelper;

return ArrayHelper::merge(include_once __DIR__ . DS . 'common.php', [
    'id' => 'web',
    'controllerNamespace' => 'app\controllers',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'default/index',
                '<action:([\w-]+)>' => 'default/<action>',
                'product/<slug:([\w-]+)>' => 'default/product',
                'category/<slug:([\w-]+)>' => 'default/category',
                '<controller:([\w-]+)>/<action:([\w-]+)>' => '<controller>/<action>',
            ],
        ],
        'request' => [
            'cookieValidationKey' => 'FDEWTvkCCWffF8w9kQ5n3h42MxDejsDs',
        ],
        'errorHandler' => [
            'errorAction' => 'default/error',
        ],
    ],
]);
