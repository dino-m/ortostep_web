<?php

use yii\helpers\ArrayHelper;

return ArrayHelper::merge(include_once __DIR__ . DS . 'common.php', [
    'id' => 'console',
    'controllerNamespace' => 'app\commands',
]);
