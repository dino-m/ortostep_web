<?php

return [
    'name' => APP_NAME,
    'version' => APP_VERSION,
    'timeZone' => APP_TIMEZONE,
    'sourceLanguage' => 'en',
    'language' => 'hr',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log'
    ],
    'components' => [
		'request'=>array(
			'enableCsrfValidation' => false,
		),	
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => 'app\models\User',
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=' . APP_DATABASE_HOST . ';port=' . APP_DATABASE_PORT . ';dbname=' . APP_DATABASE_SCHEMA,
            'username' => APP_DATABASE_USERNAME,
            'password' => APP_DATABASE_PASSWORD,
            'charset' => APP_DATABASE_CHARACTER_SET,
            'schemaCacheExclude' => [],
            'enableQueryCache' => !YII_DEBUG,
            'enableSchemaCache' => !YII_DEBUG,
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => YII_DEBUG,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'dino.mrkonjic@gmail.com',
                'password' => 'din31011989',
                'port' => 587,
                'encryption' => 'tls',
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'i18n' => [
            'translations' => [
                'app' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                    'sourceLanguage' => 'en',
                    'fileMap' => [
                        'app' => 'app.php',
                    ],
                ],
            ],
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\EmailTarget',
                    'levels' => ['error'],
                    'message' => [
                        'from' => APP_EMAIL_FROM_NO_REPLY,
                        'to' => 'admin@ortostep.hr',
                        'subject' => APP_ABSOLUTE_BASE_URL . ' - ERROR OCCURRED',
                    ],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/error.log',
                    'levels' => ['error'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/warning.log',
                    'levels' => ['warning'],
                ],
            ],
        ],
    ],
    'params' => [
        'availableLanguages' => ['hr', 'en', 'ru'],
    ],
];
