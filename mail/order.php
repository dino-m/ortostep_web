<h1>Zaprimljena narudžba</h1>

<p>
    Vrijeme zaprimanja: <?php echo date("d.m.Y") . " u " . date("H:i") ?><br>
    Naručitelj: <?php echo $contact['name'] ?> <br>
    E-mail: <?php echo $contact['email'] ?><br>
    Telefon / mobitel: <?php echo $contact['phone']; ?><br>
</p>

<table>
    <tr>
        <th>R.br.</th>
        <th>SKU</th>
        <th>Veličina</th>
        <th>Količina</th>
    </tr>

    <?php

    $i = 0;
    foreach ($cart as $item)
    {
        $i = $i + 1;
        $itemArray = explode(";", $item);

        echo "<tr><td>".$i."</td>";
        echo "<td>".$itemArray[0]."</td>";
        echo "<td>".$itemArray[1]."</td>";
        echo "<td>".$itemArray[2]."</td></tr>";
    }
	
    ?>

</table>