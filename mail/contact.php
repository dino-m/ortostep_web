<?php
/**
 * @var \yii\web\View $this
 * @var \app\models\FormContact $contact
 */

use yii\helpers\Html;

$this->title = Yii::t('app', 'Contact');
?>
<p><?= Yii::t('app', 'Date') ?>: <?= date(DATE_W3C) ?></p>
<p><?= Yii::t('app', 'Sender') ?>: <?= Html::encode($contact->email) ?></p>
<p><?= Html::encode($contact->message) ?></p>
<?php if (Yii::$app->language === 'hr'): ?>
    <p><strong>Hvala Vam!</strong></p>
    <p>Kontaktirati ćemo Vas uskoro. Kopija ovog emaila je poslana Vama i na naš kontakt email.</p>
<?php elseif (Yii::$app->language === 'en'): ?>
    <p><strong>Thank you!</strong></p>
    <p>We will contact you as soon. Copy of this email has been sent to you and to our contact email.</p>
<?php endif; ?>
