<?php

use yii\db\Migration;

class m170906_175017_Init extends Migration
{
    const LENGTH_EMAIL = 64;
    const LENGTH_SLUG = 128;

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'email' => $this->string(self::LENGTH_EMAIL)->unique()->notNull(),
            'passwordHash' => $this->string(self::LENGTH_EMAIL)->unique()->notNull(),
            'firstName' => $this->string(),
            'lastName' => $this->string(),
        ]);

        $this->createTable('newsletters', [
            'id' => $this->primaryKey(),
            'email' => $this->string(self::LENGTH_EMAIL)->unique()->notNull(),
            'dateCreated' => $this->dateTime()->notNull(),
        ]);

        $this->createTable('products', [
            'id' => $this->primaryKey(),
            'name' => $this->string(self::LENGTH_SLUG)->unique()->notNull(),
            'slug' => $this->string(self::LENGTH_SLUG)->unique()->notNull(),
            'description' => $this->text(),
            'price' => $this->float(2),
            'image' => $this->string(),
        ]);

        $this->createTable('categories', [
            'id' => $this->primaryKey(),
            'name' => $this->string(self::LENGTH_SLUG)->unique()->notNull(),
            'slug' => $this->string(self::LENGTH_SLUG)->unique()->notNull(),
            'color' => $this->string(4),
            'image' => $this->string()->defaultValue('placeholder.png'),
            'description' => $this->text(),
        ]);

        $this->createTable('productsHaveCategories', [
            'productId' => $this->integer()->notNull(),
            'categoryId' => $this->integer()->notNull(),
            'order' => $this->integer()->defaultValue(0)->notNull(),
        ]);
        $this->addPrimaryKey('productsHaveCategories-primaryKey', 'productsHaveCategories', ['categoryId', 'productId']);
        $this->addForeignKey('productsHaveCategories-products', 'productsHaveCategories', 'productId', 'products', 'id');
        $this->addForeignKey('productsHaveCategories-categories', 'productsHaveCategories', 'categoryId', 'categories', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('productsHaveCategories');
        $this->dropTable('categories');
        $this->dropTable('products');
        $this->dropTable('users');
    }
}
