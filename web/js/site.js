$(document).ready(function () {
    
    //$.mobile.loading().hide();

    //Home page covers

    /*var $res = $('div[class^=cover]'),
        i = 0,
        len = $res.length;

    $res.slice(1).hide();
    setInterval(function () {
        $res.eq(i).fadeOut(function () {
            i = (i + 1) % len
            $res.eq(i).fadeIn();
        })
    }, 4000);*/
    
    //RWD toggle menu

    $('.menu-icon').click(function () {
        $('nav').toggle();
    });
    
    /*SCROLL TO MESSAGE*/
    
    $('html, body').animate({
        scrollTop: $(".contact-fm-err, .contact-msg").offset().top
    }, 0);

    /*$('.main-carousel').flickity({
        // options
        cellAlign: 'left',
        contain: true
    });*/

    $('.news-button').click(function (e) {
        e.preventDefault();
    });
    
    //add class "active"
    
    $(function(){
        var current = location.pathname;
        $('.filter-cat-list a').each(function(){
            var $this = $(this);
            if($this.attr('href').indexOf(current) !== -1){
                $this.addClass('active');
            }
        });
    });
});