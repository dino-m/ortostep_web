<?php

define('YII_DEBUG', false);
define('YII_ENV', 'prod');
define('DS', DIRECTORY_SEPARATOR);

require_once dirname(__DIR__) . DS . 'vendor' . DS . 'autoload.php';
require_once dirname(__DIR__) . DS . 'vendor' . DS . 'yiisoft' . DS . 'yii2' . DS . 'Yii.php';
require_once dirname(__DIR__) . DS . 'config' . DS . 'bootstrap.php';

(new \yii\web\Application(require_once dirname(__DIR__) . DS . 'config' . DS . 'web.php'))->run();
