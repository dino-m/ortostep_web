<?php

return [
    'Shop' => 'Košarica',
    'Home' => 'Početna',
    'About' => 'O nama',
    'Contact' => 'Kontakt',
    'Name' => 'Naziv',
    'Message' => 'Poruka',
    'Article' => 'Artikl',
    'How to shop' => 'Način kupovine'
];
