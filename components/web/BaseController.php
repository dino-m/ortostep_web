<?php

namespace app\components\web;

use yii\web\Controller;
use Yii;

/**
 * @inheritdoc
 */
abstract class BaseController extends Controller
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $lang = strtolower(trim(Yii::$app->request->get('lang')));
        if (!empty($lang) && in_array($lang, Yii::$app->params['availableLanguages'])) {
            Yii::$app->session->set('lang', $lang);
            Yii::$app->language = $lang;
        } else {
            if (Yii::$app->session->has('lang')) {
                Yii::$app->language = Yii::$app->session->get('lang');
            }
        }
    }
}
