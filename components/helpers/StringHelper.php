<?php

namespace app\components\helpers;

use yii\helpers\BaseStringHelper;

/**
 * @inheritdoc
 */
class StringHelper extends BaseStringHelper
{
    /**
     * Generate UUID.
     * http://php.net/manual/en/function.com-create-guid.php
     * @return string
     */
    public static function createUuid()
    {
        if (function_exists('com_create_guid') === true) {
            return trim(com_create_guid(), '{}');
        }
        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }

    /**
     * Return ASCII encoded string.
     * @param string $string
     * @return string
     */
    public static function convertToAscii($string)
    {
        $ascii = '';
        for ($i = 0; $i != strlen($string); $i++) {
            $ascii .= '&#' . ord($string[$i]) . ';';
        }
        return $ascii;
    }
}
