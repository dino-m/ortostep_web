<?php

namespace app\components\helpers;

use Yii;
use yii\bootstrap\Html as BaseHtml;
use yii\web\View;

/**
 * @inheritdoc
 */
class Html extends BaseHtml
{
    /**
     * Registers multiple metaTags.
     * @param array $metaTags
     */
    public static function registerMultipleMetaTags(array $metaTags)
    {
        foreach ($metaTags as $metaTag) {
            Yii::$app->view->registerMetaTag($metaTag);
        }
    }

    /**
     * @inheritdoc
     */
    public static function flatErrorSummary($models, $nl2br = true)
    {
        $return = '';
        foreach ($models->errors as $key => $value) {
            foreach ($value as $index => $message) {
                $return .= $message . PHP_EOL;
            }
        }
        if ($nl2br) {
            return nl2br($return);
        } else {
            return $return;
        }
    }

    /**
     * @inheritdoc
     */
    public static function htmlComment($message)
    {
        return '<!--' . self::encode(nl2br($message)) . '-->';
    }

    /**
     * @inheritdoc
     */
    public static function uploadUrl($src)
    {
        return Yii::getAlias('@url-uploads') . $src;
    }

    /**
     * @inheritdoc
     */
    public static function sysImageUrl($src)
    {
        return Yii::getAlias('@system-images-url') . $src;
    }

    /**
     * @inheritdoc
     */
    public static function systemImage($src, $options = array())
    {
        return parent::img(self::sysImageUrl($src), $options);
    }

    /**
     * @inheritdoc
     */
    public static function mailtoSafe($text, $email = null, $options = [])
    {
        $text_ascii = String::convertToAscii($text);
        $email_ascii = String::convertToAscii($email);
        return parent::decode(parent::mailto($text_ascii, $email_ascii, $options));
    }

    /**
     * @inheritdoc
     */
    public static function checkboxListWithSelectAll($name, $selection = null, $items = [], $options = [])
    {
        return self::registerSelectAllSwitchJSAndCheckbox($name) . self::hiddenInput("{$name}[]") . parent::checkboxList($name, $selection, $items, $options);
    }

    /**
     * @inheritdoc
     */
    public static function checkboxList($name, $selection = null, $items = [], $options = [])
    {
        return self::hiddenInput("{$name}[]") . parent::checkboxList($name, $selection, $items, $options);
    }

    /**
     * @inheritdoc
     */
    public static function activeCheckboxListWithSelectAll($model, $attribute, $items, $options = [])
    {
        return self::registerSelectAllSwitchJSAndCheckbox($attribute) . self::hiddenInput("{$attribute}[]") . parent::activeCheckboxList($model, $attribute, $items, $options);
    }

    /**
     * @inheritdoc
     */
    public static function activeCheckboxList($model, $attribute, $items, $options = [])
    {
        return self::hiddenInput("{$attribute}[]") . parent::activeCheckboxList($model, $attribute, $items, $options);
    }

    /**
     * Registers JS for select all function and returns HTML code for checkbox that does that.
     * @param string $name Name of array that needs to be checked all.
     * @return string
     */
    static function registerSelectAllSwitchJSAndCheckbox($name)
    {
        Yii::$app->view->registerJs('
        var selected_checkboxes = [];
        function selectAllSwitch(input_name, default_state) {
            //check if input_name has been called already
            if (!(input_name in selected_checkboxes)) {
                selected_checkboxes[input_name] = default_state;
            }
            //doing the actual checking and setting new status
            if (selected_checkboxes[input_name]) {
                jQuery("input[name=\'" + input_name + "[]\']").each(function () {
                    this.checked = false;
                });
            } else {
                jQuery("input[name=\'" + input_name + "[]\']").each(function () {
                    this.checked = true;
                });
            }
            selected_checkboxes[input_name] = !selected_checkboxes[input_name];
        }', View::POS_END, 'select-all-js-switch');

        return self::checkbox('', false, [
            'class' => 'select-all-checkbox',
            'data' => $name,
            'onclick' => "selectAllSwitch(this.getAttribute('data'), false)",
            'label' => Yii::t('app', 'Select/Deselect All'),
        ]);
    }
}
