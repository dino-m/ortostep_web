<?php
/**
 * @var $this \yii\web\View
 */

use yii\helpers\Html;

?>
<meta charset="<?= Yii::$app->charset ?>"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta http-equiv="Content-Type" content="html/text" charset="UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=1"/>
<title><?= Html::encode($this->title) ?></title>
<meta name="keywords" content="cipele, obuca, medicina, ortopedska, obuca, Ortostep, Osijek">
<meta name="description" content="Ortostep - proizvodnja medicinskih i ortopedskih pomagala i obuće">
<?= Html::csrfMetaTags() ?>
<?php $this->head() ?>
