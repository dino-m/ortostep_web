<?php
/**
 * @var $this \yii\web\View
 */

use yii\helpers\Url;
use yii\helpers\Html;

?>
<header>
    <div class="width-1200">
        <section class="logo-header">
            <a href="<?= Url::to(['/']) ?>"><?= Html::img(Yii::getAlias('@urlRootImage').'/'.'logo.png'); ?></a>
        </section>
        
        <a href="<?= Url::to(['/cart']) ?>"><?= Html::img(Yii::getAlias('@urlRootImage').'/'.'shop-icon.png', ['class'=>'shop-icon-mob','width'=>'25px', 'height'=>'25px']); ?></a>        
        <?= Html::img(Yii::getAlias('@urlRootImage').'/'.'menu_icon.png', ['class' => 'menu-icon', 'width'=>'30px', 'height'=>'30px']); ?>
        
        <nav>
            <ul>
                <li><a href="<?= Url::to(['/about']) ?>"><?= Yii::t('app', 'About') ?></a></li>
                <li><a href="<?= Url::to(['/how-to-shop']) ?>"><?= Yii::t('app', 'How to shop') ?></a></li>
                <li><a href="<?= Url::to(['/contact']) ?>"><?= Yii::t('app', 'Contact') ?></a></li>
                <li><a href="<?= Url::to(['/cart']) ?>"><?= Html::img(Yii::getAlias('@urlRootImage').'/'.'shop-icon.png', ['class'=>'shop-icon','width'=>'25px', 'height'=>'25px']); ?></a></li>
            </ul>
        </nav>
    </div>
</header>
