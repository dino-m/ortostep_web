<?php
/**
 * @var $this \yii\web\View
 * @var $content string
 */

use app\assets\CommonAsset;
use app\components\widgets\Alert;
use yii\widgets\Spaceless;

?>
<?php CommonAsset::register($this) ?>
<?php Spaceless::begin() ?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <?= $this->render('head') ?>
</head>
<body>
<?php $this->beginBody() ?>
<?= $this->render('navigation') ?>
<!--<div class="container">
    <?= Alert::widget() ?>
</div>-->
<div class="container">
    <?= $content ?>
</div>
<?= $this->render('footer') ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
<?php Spaceless::end() ?>
