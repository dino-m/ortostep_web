<?php
/**
 * @var $this \yii\web\View
 * @var $product \app\models\Products
 */

use yii\helpers\Html;
use yii\helpers\Url;
$this->title = $product->sku;
?>

<div class="banner-top"><?= Html::img(Yii::getAlias('@urlBannerImage').'/'.$category->image); ?></div>
<section class="pr-nav">
    <nav>
        <a href="<?= Url::to(['/']) ?>">Naslovna</a> &nbsp;&nbsp;>&nbsp;&nbsp; 
       <a href="<?= Url::to(['category/' .$category->slug]) ?>"> <?= Html::encode($category->name) ?> </a> &nbsp;&nbsp;>&nbsp;&nbsp; 
       <?= Html::encode($product->collection) ?> <?= Html::encode($product->sku) ?>
   </nav>  
</section>
<section class="pr-main"> 
    <div class="pr-img">
        <?= Html::img(Yii::getAlias('@urlCategoryImage').'/'. $product->image); ?>
    </div>
    <form name="cart-item" action="../cart" method="post">
    <input type="hidden" name="addItem" value="yes">
    <section class="pr-detail">
        <h1><?= Html::encode($product->collection) ?> <?= Html::encode($product->sku) ?></h1>
        <table>
            <tr>
                <td><p>SKU:</p></td>
                <td>
                    <p>
                        <b><?= Html::encode($product->sku) ?></b>
                        <input type="hidden" name="product-sku" value="<?= Html::encode($product->sku) ?>">
                    </p>
                </td>
            </tr>
            <tr>
                <td><p>Dostupnost:</p></td>
                <td><p><b>Na stanju</b></p></td>
            </tr>
            <tr>
                <td><p>Veličina:</p></td>
                <td>
                    <select name="size">
                    <?php foreach($sizes as $size): ?>
                    <option value="<?= Html::encode($size->numb) ?>"><?= Html::encode($size->numb) ?></option>
                     <?php endforeach; ?>   
                    </select>
                </td>
            </tr>

            <tr>
                <td><p>Količina:</p></td>
                <td><input type="number" name="qty" value="1"></td>
            </tr>            
        </table>
        <section class="pr-remark">
            <p>Odabirom ovog proizvoda kreirate dokument narudžbe koju u zadnjem koraku šaljete nama, a mi ćemo Vam u najkraće mogućem roku odgovoriti
            sa ponudom kreiranom isključivo za Vas.</p>
        </section>
        
        <section class="pr-bt-sc">

            <input class="pr-cart-bt" type="submit" value="Dodaj u narudžbu">

            <div class="pr-share">
                <i class="fab fa-facebook-square" aria-hidden="true" style="font-size:40px"></i>
                <i class="fab fa-instagram" aria-hidden="true" style="font-size:40px"></i>
                <i class="fab fa-google-plus-square" aria-hidden="true" style="font-size:40px"></i>
                <i class="fab fa-twitter-square" aria-hidden="true" style="font-size:40px"></i>
            </div>
        </section>
    </section>
	<input type="hidden" name="image" value="<?= Html::encode($product->image) ?>"/>
	<input type="hidden" name="desc" value="<?= Html::encode($product->description) ?>"/>
        <input type="hidden" name="collect" value="<?= Html::encode($product->collection) ?>"/>
    </form>
    <article class="pr-desc">
        <h1>OPIS PROIZVODA</h1>
        <p><?= Html::encode($product->description) ?></p>
    </article>
</section>
<section class="sc-main">
    <h1><span class="heading-border">Pratite nas</span></h1>
    <div class="sc-icons">
        <a href="#"><div class="sc1"><i class="fab fa-facebook-f" aria-hidden="true" style="font-size:50px"></i></div></a>
        <a href="#"><div class="sc2"><i class="fab fa-instagram" aria-hidden="true" style="font-size:50px"></i></div></a>
        <a href="#"><div class="sc3"><i class="fab fa-twitter" aria-hidden="true" style="font-size:50px"></i></div></a>
        <a href="#"><div class="sc4"><i class="fab fa-google-plus-g" aria-hidden="true" style="font-size:50px"></i></div></a>
    </div>
</section>
