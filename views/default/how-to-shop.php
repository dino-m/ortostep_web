<?php
/**
 * @var $this yii\web\View
 * @var $category \app\models\Categories
 * @var $products \app\models\Products[]
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
$this->title = Yii::t('app', 'How to shop');
?>

<div class="banner-head htb-head">
    <!--<h1>Mi kreiramo za Vas najbolju ponudu</h1>-->
</div>

<article class="ab-article">
    <h1><span class="heading-border">KAKO KUPOVATI?</span></h1>
    <p>Ortostep web stranica je predviđena za kreiranje narudžbi koje istog trenutka zaprima odjel za prodaju nakon Vašeg slanja. Ono što je potrebno
        je da stavite u košaricu sve što biste željeli naručiti, te po završetku Vaše kupovine upišete svoje podatke u predviđenu formu u košarici
        i pošaljete narudžbu. Narudžba se kreira u obliku PDF dokumenta koji prima naš odjel, a kopiju primate Vi na Vašu email adresu.
        Nakon primitka Vaše narudžbe u najkraćem mogućem roku odgovorit ćemo Vam s najboljom ponudom kreiranom isključivo za Vas.
        Načine plaćanja i načine dostave dogovarate isključivo s našim odjelom prodaje koji će s Vama dogovoriti najpovoljniju i nabolju opciju.
        Za sva ostala pitanja možete nam se obratiti putem dostupnih <a href="<?= Url::to(['/contact']) ?>">kontakt informacija</a>.
    </p>
</article>

<?php if (!$contact->success && !$contact->hasErrors()): ?>
<section class="contact-fm">
    <h1><span class="heading-border">Kontaktirajte nas</span></h1>
    <article>
        <?php $form = ActiveForm::begin(['id'=>'contactform', 'options' => ['type' => 'post'], 'action' => ['/about']]); ?>
            <div class="contact-info-group">
                <?= $form->field($contact, 'name')->textInput(['placeholder'=>'Ime i prezime']) ?>
                <?= $form->field($contact, 'email')->textInput(['placeholder'=>'Email']) ?>
                <?= $form->field($contact, 'phone')->textInput(['placeholder'=>'Telefon']) ?>
                <?= $form->field($contact, 'message')->textarea(['rows' => 5, 'placeholder'=>'Poruka']) ?>
            </div>
            <p>Molimo Vas da prije slanja poruke popunite sva polja</p>
            <button class="sendbutton" type="submit"><?= Yii::t('app', 'Pošalji') ?></button>
        <?php ActiveForm::end() ?> 
    </article>
</section>

<?php elseif(!$contact->success && $contact->hasErrors()): ?>
<section class="contact-fm contact-fm-err">
    <h1><span class="heading-border">Kontaktirajte nas</span></h1>
    <article>
        <?php $form = ActiveForm::begin(['id'=>'contactform', 'options' => ['type' => 'post', 'name'=>'contactform'], 'action' => ['/about']]); ?>
            <div class="contact-info-group">
                <?= $form->field($contact, 'name')->textInput(['placeholder'=>'Ime i prezime']) ?>
                <?= $form->field($contact, 'email')->textInput(['placeholder'=>'Email']) ?>
                <?= $form->field($contact, 'phone')->textInput(['placeholder'=>'Telefon']) ?>
                <?= $form->field($contact, 'message')->textarea(['rows' => 5, 'placeholder'=>'Poruka']) ?>
            </div>
            <p>Molimo Vas da prije slanja poruke popunite sva polja</p>
            <button class="sendbutton" type="submit"><?= Yii::t('app', 'Pošalji') ?></button>
        <?php ActiveForm::end() ?> 
    </article>
</section>

<?php else: ?>
    <?php if (Yii::$app->language === 'hr'): ?>
    <section class="contact-msg">
            <h1>Hvala Vam!</h1>
            <p>Vaša poruka je uspješno poslana, kao i kopija na Vaš email.</p>
            <p>Kontaktirati ćemo Vas uskoro.</p>
        <?php elseif (Yii::$app->language === 'en'): ?>
            <h1>Thank you!</h1>
            <p>Your message has been successfully sent, along with a copy to email you entered.</p>
            <p>We will contact you back soon.</p>
        <?php endif; ?>
    </section>
<?php endif ?>  
        
<section class="sc-main">
    <h1><span class="heading-border">Pratite nas</span></h1>
    <div class="sc-icons">
        <a href="#"><div class="sc1"><i class="fab fa-facebook-f" aria-hidden="true" style="font-size:50px"></i></div></a>
        <a href="#"><div class="sc2"><i class="fab fa-instagram" aria-hidden="true" style="font-size:50px"></i></div></a>
        <a href="#"><div class="sc3"><i class="fab fa-twitter" aria-hidden="true" style="font-size:50px"></i></div></a>
        <a href="#"><div class="sc4"><i class="fab fa-google-plus-g" aria-hidden="true" style="font-size:50px"></i></div></a>
    </div>
</section>



