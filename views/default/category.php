<?php
/**
 * @var $this \yii\web\View
 * @var $category \app\models\Categories
 * @var $products \app\models\Products[]
 * @var $pagination \yii\data\Pagination
 */

use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
?>


<div class="banner-top"><?= Html::img(Yii::getAlias('@urlBannerImage').'/'.$category->image); ?></div>

<div class="for-middle">
<section class="content-mid">
    <nav><a href="<?= Url::to(['/']) ?>">Naslovna</a> &nbsp;&nbsp;>&nbsp;&nbsp; <?= Html::encode($category->name) ?></nav>
    <section class="filter-cont"> 
        <h3>KATEGORIJE:</h3>
        <section class="filter-cat-list">
            <ul>
                <li><a href="<?= Url::to(['/category/anatomska-medistep']) ?>">Anatomska i Medistep obuća</a></li>
                <li><a href="<?= Url::to(['/category/diastep']) ?>">Diastep</a></li>
                <li><a href="<?= Url::to(['/category/radnaobuca']) ?>">Radna obuća</a></li>
                <li><a href="<?= Url::to(['/category/fashion']) ?>">Fashion</a></li>
            </ul>
        </section>
		<?php if(empty($collection) == false){ ?>
        <h3>KOLEKCIJE:</h3>
        <section class="filter-cat-list"> 
            <ul>
                <?php foreach($collection as $c): ?>
                <li><a href="/category/<?= Html::encode($category->slug) ?>?collection=<?= Html::encode($c->collection) ?>"><?= Html::encode($c->collection) ?></a></li>
                <?php endforeach; ?>
            </ul>
        </section>
		<?php } ?>
    </section>
    <div class="products-cont">
        <div class="products-title">
            <h1><?= Html::encode($category->name) ?></h1>
        </div>
        <?php foreach ($products as $p): ?>
            <div class="sg-prod-cont">
                <?= Html::img(Yii::getAlias('@urlCategoryImage').'/'. $p->image); ?></br>
                <h2><?= Yii::t('app', 'Article') ?>: <?= Html::encode($p->collection) ?> <?= Html::encode($p->sku) ?></h2>
                <a href="<?= Url::to(['/product/'. $p->slug ]) ?>"><div class="product-more">Više</div></a>
            </div>
        <?php endforeach; ?>

        <div class="pagination-main">
            <?= LinkPager::widget([
                'pagination' => $pagination, 
                'maxButtonCount' => 3,
                'activePageCssClass' => 'active'
                ])
            ?>
        </div>
    </div>
</section>
    </div>
<section class="newsletter">
    <h1><span class="heading-border">NEWSLETTER</span></h1>
    <article>
        <h3>Saznaj na vrijeme za ono najbolje!</h3>
        <div id="mc_embed_signup">
            <form action="https://ortostep.us17.list-manage.com/subscribe/post?u=42874850678ec0fd74567df20&amp;id=2a776a412f" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <div id="mc_embed_signup_scroll">
                    <input type="email" value="" name="EMAIL" class="required email" id="news-input" placeholder="Email">
                    <button type="submit" name="subscribe" id="mc-embedded-subscribe" class="button news-button" onclick="FormReset()">Prijavi se!</button>
                </div>
            </form>
        </div>
    </article>
</section>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
<script type='text/javascript'>
    (function($) {
        window.fnames = new Array();
        window.ftypes = new Array();
        fnames[0]='EMAIL';
        ftypes[0]='email';
        fnames[1]='MMERGE1';
        ftypes[1]='text';
    }
    (jQuery));
    var $mcj = jQuery.noConflict(true);
    
    function FormReset(){
        document.getElementById('mc-embedded-subscribe-form').reset();
        document.getElementById('news-input').style.border = 'none';
    }
</script>
