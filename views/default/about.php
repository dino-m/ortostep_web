<?php
/**
 * @var $this yii\web\View
 * @var $category \app\models\Categories
 * @var $products \app\models\Products[]
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
$this->title = Yii::t('app', 'About');
?>

<div class="banner-head ab-head">
    <h1>Mi kreiramo obuću samo za Vas</h1>
</div>

<article class="ab-article">
    <h1><span class="heading-border">ORTOSTEP?</span></h1>
    <p>MI PROIZVODIMO OBUĆU. KVALITETNU, FUNKCIONANU I UDOBNU. PROIZVODIMO ŠTO VOLIMO. VOLIMO ŠTO PROIZVODIMO.</p>
    <p>12 godina izrađujemo najzahtjevniju individualno izrađenu ortopedsku obuću. Sve je ručni rad. Koristimo prirodne materijale. Izgradili smo tim vrhunskih stručnjaka.
    POSTOJI SVE VIŠE PROIZVOĐAČA OBUĆE KOJIMA JE JEDINI CILJ PROFIT.
    MI NISMO JEDNI OD NJIH. Ortostep obuća stvorena je iz pasioniranog uvjerenja, da bi ljudi trebali prestati pristajati na kompromise kada je obuća u pitanju, i da trebaju težiti apsolutnoj udobnosti. Zahvaljujući Ortostepu , udobnost i zdravlje nikada nisu tako dobro izgledali zajedno.</p>
    <h3>POVIJEST TVRTKE</h3>
    <p>1982. Nikola i Elizabeta Kolarić pokreću obiteljsku tvrtku Kolarić izrada kožne galanterije. Tijekom godina, bave se proizvodnjom remena, kožnih torbi, kožne konfekcije i modne obuće. 
    Modna marka Leks shoes, te ostali proizvodi od kože bili su dostupni diljem Hrvatske. u svojim proizvodnim pogonima proizvodili su obuću za Camel, Esprit, Clarsk, Gas blue jeans i dr. Elizabeta i Nikola 1996. izgrađuju novo proizvodno postrojenje na površni od 1500m2. 
    Početkom milenija u posao se uključuje njihov sin Damir sa suprugom Marjanom. Uočivši promjene u okruženju, mlađa generacija odlučuje zaokrenuti smjer poslovanja , te se od tada tvrtka usavršava i specijalizira za proizvodnju ortopedske obuće.
    Uspješna obiteljska priča traje neprekidno posljednjih 35 godina.</p>
</article>

<section class="ab-ban">
    <h1><b>ORTO</b> – {grč. orthos} predmetak u složenicma sa značenjem: pravi, pravilan, uspravan;</h1>
    <h1><b>STEP</b> – korak {& fig.} koračati, način koračanja</h1>
</section>

<section class="ab-work">
    <h1><span class="heading-border">Način na koji radimo</span></h1>
    <div class="ab-work-article">
        <div class="ab-pic1"></div>
        <p><b>Individualne cipele po mjeri </b></br>Tvrtka Ortostep ugovorni je isporučitelj  individualno izrađene obuće za osiguranike HZZO od 2005. Tijekom tih 13 godina proizveli smo desetke tisuća pari ortopedske obuće, susreli smo se sa nizom zahtjevnih deformiteta, 
            i puno naučili slušajući svoje kupce. Mnogima smo pomogli da unatoč deformitetima stopala lakše obavljaju svoje svakodnevne aktivnosti.
            Danas ortopedsku obuću po mjeri možete naručiti kod naših suradnika, ugovornih isporučitelja Hrvatskog zavoda za zdravstveno osiguranje: 
            <a href="http://salvushealth.com/hr">Solvus</a>, 
            <a href="http://www.ottobock.hr/">Ottobock</a>,
            <a href="https://www.svkatarina.hr/hr">Ortopedija 2000</a>,
            KDK,
            <a href="http://protetikamodular.hr/">Protetika modular</a>,
            Step d.o.o. Orebić</p>
    </div>
    <div class="ab-work-article">
        <div class="ab-pic2"></div>
        <p><b>Serijski izrađena cipela po tipu bolesti</b></br>Postoji cijeli niz problema sa stopalima koji nisu u sustavu HZZO. Svoje znanje i 
            iskustvo primjenjujemo u serijskoj proizvodnji obuće, te smo stvorili linije obuće za osobe koje boluju od dijabetesa, halux valgusa, 
            petnih trnova, spuštenih stopala i sl.</p>
    </div>
</section>

<?php if (!$contact->success && !$contact->hasErrors()): ?>
<section class="contact-fm">
    <h1><span class="heading-border">Zanima vas više?</span></h1>
    <article>
        <?php $form = ActiveForm::begin(['id'=>'contactform', 'options' => ['type' => 'post'], 'action' => ['/about']]); ?>
            <div class="contact-info-group">
                <?= $form->field($contact, 'name')->textInput(['placeholder'=>'Ime i prezime']) ?>
                <?= $form->field($contact, 'email')->textInput(['placeholder'=>'Email']) ?>
                <?= $form->field($contact, 'phone')->textInput(['placeholder'=>'Telefon']) ?>
                <?= $form->field($contact, 'message')->textarea(['rows' => 5, 'placeholder'=>'Poruka']) ?>
            </div>
            <p>Molimo Vas da prije slanja poruke popunite sva polja</p>
            <button class="sendbutton" type="submit"><?= Yii::t('app', 'Pošalji') ?></button>
        <?php ActiveForm::end() ?> 
    </article>
</section>

<?php elseif(!$contact->success && $contact->hasErrors()): ?>
<section class="contact-fm contact-fm-err">
    <h1><span class="heading-border">Javite nam se</span></h1>
    <article>
        <?php $form = ActiveForm::begin(['id'=>'contactform', 'options' => ['type' => 'post', 'name'=>'contactform'], 'action' => ['/about']]); ?>
            <div class="contact-info-group">
                <?= $form->field($contact, 'name')->textInput(['placeholder'=>'Ime i prezime']) ?>
                <?= $form->field($contact, 'email')->textInput(['placeholder'=>'Email']) ?>
                <?= $form->field($contact, 'phone')->textInput(['placeholder'=>'Telefon']) ?>
                <?= $form->field($contact, 'message')->textarea(['rows' => 5, 'placeholder'=>'Poruka']) ?>
            </div>
            <p>Molimo Vas da prije slanja poruke popunite sva polja</p>
            <button class="sendbutton" type="submit"><?= Yii::t('app', 'Pošalji') ?></button>
        <?php ActiveForm::end() ?> 
    </article>
</section>

<?php else: ?>
    <?php if (Yii::$app->language === 'hr'): ?>
    <section class="contact-msg">
            <h1>Hvala Vam!</h1>
            <p>Vaša poruka je uspješno poslana, kao i kopija na Vaš email.</p>
            <p>Kontaktirati ćemo Vas uskoro.</p>
        <?php elseif (Yii::$app->language === 'en'): ?>
            <h1>Thank you!</h1>
            <p>Your message has been successfully sent, along with a copy to email you entered.</p>
            <p>We will contact you back soon.</p>
        <?php endif; ?>
    </section>
<?php endif ?>  
        
<section class="sc-main">
    <h1><span class="heading-border">Pratite nas</span></h1>
    <div class="sc-icons">
        <a href="#"><div class="sc1"><i class="fab fa-facebook-f" aria-hidden="true" style="font-size:50px"></i></div></a>
        <a href="#"><div class="sc2"><i class="fab fa-instagram" aria-hidden="true" style="font-size:50px"></i></div></a>
        <a href="#"><div class="sc3"><i class="fab fa-twitter" aria-hidden="true" style="font-size:50px"></i></div></a>
        <a href="#"><div class="sc4"><i class="fab fa-google-plus-g" aria-hidden="true" style="font-size:50px"></i></div></a>
    </div>
</section>



