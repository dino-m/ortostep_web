<?php
/**
 * @var $this yii\web\View
 */


use yii\grid\GridView;
use yii\data\ActiveDataProvider;

$this->title = Yii::t('app', 'Cart');
?>

<div class="ct-head banner-head ab-head">
    <!--<h1>Pošaljite svoju narudžbu za najbolju ponudu.</h1>-->
</div>

<section class="ct-main">
    <h1><span class="heading-border">VAŠA KOŠARICA</span></h1>
<?php
    $i = 0;
    if (isset($_SESSION['cart']) and sizeof($_SESSION['cart']) > 0)
    {
        echo "<div class='ct-main-tb'><table><tr><th>R.br.</th><th>Kolekcija</th><th>SKU</th><th>Veličina</th><th>Količina</th></tr>";

        foreach ($_SESSION['cart'] as $item)
        {
            $i = $i + 1;
            $itemArray = explode(";", $item);

            echo "<tr><td>".$i."</td>";
            echo "<td>".$itemArray[5]."</td>";
            echo "<td>".$itemArray[0]."</td>";
            echo "<td>".$itemArray[1]."</td>";
            echo "<td class='ct-td-fix'><form action='cart' method='post'><input type='hidden' name='updateItemQty' value='".($i-1)."'/><input class='ct-qty' type='number' name='newQty' min='0' value='".$itemArray[2]."'/><input class='ct-bt-qty' type='submit' value='Osvježi'/></form></td>";
            echo "<td><form action='cart' method='post'><input type='hidden' name='removeItem' value='".($i-1)."'/><input class='ct-bt-del' type='submit' value='Briši'/></form></td></tr>";
        }

        echo "</table></div>";
    }
    else
    {
        echo "Košarica je prazna.";
    }
?>

<?php
    if ($i > 0)
    {
?>
    <form action="cart" method="post">
        <input type="hidden" name="emptyCart" value="yes"/>
        <button id="ct-emp" type="submit">Isprazni košaricu</button>
    </form>

    <form id="contactform" action="cart" method="post">
        <div class="contact-info-group">
            <input type="hidden" name="sendOrder" value="yes"/>
            <br><br>
            <input type="text" name="name" placeholder="Ime i prezime">
            <br>
            <input type="email" required="" name="email" placeholder="E-mail">
            <br>
            <?php if (isset($_SESSION['sendOrderValidationError'])) {
                echo '<p>Niste unijeli e-mail adresu.</p>';
            } ?>
            <input type="text" name="phone" placeholder="Telefon ili mobitel">
            <br><br>
        </div>
        <button class="sendbutton" type="submit">Pošalji narudžbu</button>
        <br><br>
    </form>

<?php
    }
?>

<?php if (isset($_SESSION['sendOrderSuccess'])) {
    echo '<p><br>
        Zaprimili smo Vašu narudžbu i na Vašu e-mail adresu poslali kopiju iste.
        Uskoro će Vam se Ortostep javiti s najboljom ponudom.<br>
        Hvala Vam na povjerenju!
        </p>';
    unset($_SESSION['sendOrderSuccess']);
} ?>

</section>

<section class="sc-main">
    <h1><span class="heading-border">Pratite nas</span></h1>
    <div class="sc-icons">
        <a href="#"><div class="sc1"><i class="fab fa-facebook-f" aria-hidden="true" style="font-size:50px"></i></div></a>
        <a href="#"><div class="sc2"><i class="fab fa-instagram" aria-hidden="true" style="font-size:50px"></i></div></a>
        <a href="#"><div class="sc3"><i class="fab fa-twitter" aria-hidden="true" style="font-size:50px"></i></div></a>
        <a href="#"><div class="sc4"><i class="fab fa-google-plus-g" aria-hidden="true" style="font-size:50px"></i></div></a>
    </div>
</section>