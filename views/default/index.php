<?php
/**
 * @var $this \yii\web\View
 */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use cinghie\cookieconsent\widgets\CookieWidget;
$this->title = Yii::t('app', 'Home');
?>

<!--<section class="fp-gallery">
    <div class="main-gallery js-flickity">
        <div class="carousel-cell">
            <img src="img/covers/cover-back-1.jpg">
        </div>
        <div class="carousel-cell">
            <img src="img/covers/cover-back-2.jpg">
        </div>
        <div class="carousel-cell">
            <img src="img/covers/cover-back-3.jpg">
        </div>
    </div>
</section>-->

<div class="carousel" data-flickity='{ "fullscreen": true, "lazyLoad": 1 }'>
  <div class="carousel-cell">
    <img class="carousel-cell-image"
      data-flickity-lazyload="img/covers/cover-back-1.jpg" />
  </div>
  <div class="carousel-cell">
    <img class="carousel-cell-image"
      data-flickity-lazyload="img/covers/cover-back-2.jpg" />
  </div>
  <div class="carousel-cell">
    <img class="carousel-cell-image"
      data-flickity-lazyload="img/covers/cover-back-3.jpg" />
  </div>
</div>

<section class="fp-category">
    <h1><span class="heading-border">KATEGORIJE</span></h1>
    <div class="fp-category-list">
        <div class="fp-zoom">
            <a href="<?= Url::to(['/category/anatomska-medistep']) ?>">
                <div class="category-1">
                    <div class="cat-overlay"></div>
                </div>
                <div class="cat-name-1"><p>Anatomska i Medistep obuća</p></div>
            </a>
        </div>
        <div class="fp-zoom">
            <a href="<?= Url::to(['/category/diastep']) ?>">
                <div class="category-2">
                    <div class="cat-overlay"></div>
                </div>
                <div class="cat-name-2"><p>Diastep</p></div>
            </a>
        </div>
        <div class="fp-zoom">
            <a href="<?= Url::to(['/category/radnaobuca']) ?>">
                <div class="category-3">
                    <div class="cat-overlay"></div>
                </div>
                <div class="cat-name-3"><p>Radna obuća</p></div>
            </a>
        </div>
        <div class="fp-zoom">
            <a href="<?= Url::to(['/category/fashion']) ?>">
                <div class="category-4">
                    <div class="cat-overlay"></div>
                </div>
                <div class="cat-name-4"><p>Fashion</p></div>
            </a>
        </div>
    </div>
</section>
<section class="fp-shops">
    <div class="shops-overlay"></div>
    <h1><span class="heading-border">PRODAJNA MJESTA</span></h1>
    <article>
        <div class="shops-location1">
            <img src="img/location_icon.png" width="80" height="80"/>
            <div class="location1-address">
                <p><b>OSIJEK (31000)</b></br>Županijska 20</p>
            </div>
        </div>
        <div class="shops-location2">
            <img src="img/location_icon.png" width="80" height="80"/>
            <div class="location2-address">
                <p><b>ZAGREB (10000)</b></br>Nemčićeva 6</p>
            </div>
        </div>
        <div class="shops-location3">
            <img src="img/location_icon.png" width="80" height="80"/>
            <div class="location3-address">
                <p><b>ZAGREB (10000)</b></br>Shopping centar Prečko</p>
            </div>
        </div>
        <a href="<?= Url::to(['/contact']) ?>">
            <div class="button-location">Detaljnije</div>
        </a>
    </article>
</section>
<section class="fp-download">
    <h1><span class="heading-border">KATALOZI</span></h1>
    <article>
        <h3>Preuzmite i pregledajte naše najnovije kataloge!</h3>
        <section class="download-row">
            <a href="/download/katalog_anatomske_obuce.pdf" download>
                <?= Html::img(Yii::getAlias('@urlDownloadImage').'/'.'anatomska_obuca.png',['class'=>'download-img']); ?>
                <p>Anatomska obuća<br> <i class="fas fa-download"></i></p>
            </a>
            <a href="/download/katalog_medistep_obuce.pdf" download>
                <?= Html::img(Yii::getAlias('@urlDownloadImage').'/'.'medistep_obuca.png',['class'=>'download-img']); ?>
                <p>Medistep obuća<br> <i class="fas fa-download"></i></p>
            </a>
            <a href="/download/katalog_ortopedske_obuce.pdf" download>
                <?= Html::img(Yii::getAlias('@urlDownloadImage').'/'.'ortopedska_obuca.png',['class'=>'download-img']); ?>
                <p>Ortopedska obuća<br> <i class="fas fa-download"></i></p>
            </a>
            <a href="/download/katalog_zastitne_odjece.pdf" download>
                <?= Html::img(Yii::getAlias('@urlDownloadImage').'/'.'zastitna_odjeca.png',['class'=>'download-img']); ?>
                <p>Zaštitna odjeća<br> <i class="fas fa-download"></i></p>
            </a>
        </section>
    </article>
</section>
<!--<section class="fp-testimonials">
    <h1><span class="heading-border">DRUGI O NAMA</span></h1>
    <section class="testimonials-content">
        <section class="testimonial1">
            <div class="face1"></div>
            <figure class="fig1">
                <blockquote><i>"I'm saying something nice about your company, nice work, everything is great!"</i>
                </blockquote>
                <footer>
                    <cite>Dipl. ing. Tomy Parkins</cite>
                </footer>
            </figure>
        </section>
        <section class="testimonial2">
            <div class="face2"></div>
            <figure class="fig2">
                <blockquote><i>"I'm the other person saying something nice about your company, nice work, everything
                        is great!"</i></blockquote>
                <footer>
                    <cite>Mr. Marko Markich</cite>
                </footer>
            </figure>
        </section>
        <section class="testimonial3">
            <div class="face3"></div>
            <figure class="fig3">
                <blockquote><i>"I'm the third person saying something nice about your company, nice work, everything
                        is great!"</i></blockquote>
                <footer>
                    <cite>Mr. Ivan Ivanovich</cite>
                </footer>
            </figure>
        </section>

        <div class="testimonial-navigation">
            <div class="testimonial-buttons">
                <div class="left-button"></div>
                <div class="center-button"></div>
                <div class="right-button"></div>
            </div>
        </div>
    </section>-->
<?php if (!$contact->success && !$contact->hasErrors()): ?>
<section class="contact-fm">
    <h1><span class="heading-border">Javite nam se</span></h1>
    <article>
        <?php $form = ActiveForm::begin(['id'=>'contactform', 'options' => ['type' => 'post', 'name'=>'contactform'], 'action' => ['/index']]); ?>
            <div class="contact-info-group">
                <?= $form->field($contact, 'name')->textInput(['placeholder'=>'Ime i prezime']) ?>
                <?= $form->field($contact, 'email')->textInput(['placeholder'=>'Email']) ?>
                <?= $form->field($contact, 'phone')->textInput(['placeholder'=>'Telefon']) ?>
                <?= $form->field($contact, 'message')->textarea(['rows' => 5, 'placeholder'=>'Poruka']) ?>
            </div>
            <p>Molimo Vas da prije slanja poruke popunite sva polja</p>
            <button class="sendbutton" type="submit"><?= Yii::t('app', 'Pošalji') ?></button>
        <?php ActiveForm::end() ?> 
    </article>
</section>

<?php elseif(!$contact->success && $contact->hasErrors()): ?>
<section class="contact-fm contact-fm-err">
    <h1><span class="heading-border">Javite nam se</span></h1>
    <article>
        <?php $form = ActiveForm::begin(['id'=>'contactform', 'options' => ['type' => 'post', 'name'=>'contactform'], 'action' => ['/index']]); ?>
            <div class="contact-info-group">
                <?= $form->field($contact, 'name')->textInput(['placeholder'=>'Ime i prezime']) ?>
                <?= $form->field($contact, 'email')->textInput(['placeholder'=>'Email']) ?>
                <?= $form->field($contact, 'phone')->textInput(['placeholder'=>'Telefon']) ?>
                <?= $form->field($contact, 'message')->textarea(['rows' => 5, 'placeholder'=>'Poruka']) ?>
            </div>
            <p>Molimo Vas da prije slanja poruke popunite sva polja</p>
            <button class="sendbutton" type="submit"><?= Yii::t('app', 'Pošalji') ?></button>
        <?php ActiveForm::end() ?> 
    </article>
</section>

<?php else: ?>
    <?php if (Yii::$app->language === 'hr'): ?>
    <section class="contact-msg">
            <h1>Hvala Vam!</h1>
            <p>Vaša poruka je uspješno poslana, kao i kopija na Vaš email.</p>
            <p>Kontaktirati ćemo Vas uskoro.</p>
        <?php elseif (Yii::$app->language === 'en'): ?>
            <h1>Thank you!</h1>
            <p>Your message has been successfully sent, along with a copy to email you entered.</p>
            <p>We will contact you back soon.</p>
        <?php endif; ?>
    </section>
<?php endif ?>

<section class="newsletter">
    <h1><span class="heading-border">NEWSLETTER</span></h1>
    <article>
        <h3>Saznaj na vrijeme za ono najbolje!</h3>
        <div id="mc_embed_signup">
            <form action="https://ortostep.us17.list-manage.com/subscribe/post?u=42874850678ec0fd74567df20&amp;id=2a776a412f" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <div id="mc_embed_signup_scroll">
                    <input type="email" value="" name="EMAIL" class="required email" id="news-input" placeholder="Email">
                    <button type="submit" name="subscribe" id="mc-embedded-subscribe" class="button news-button" onclick="FormReset()">Prijavi se!</button>
                </div>
            </form>
        </div>
    </article>
</section>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
<script type='text/javascript'>
    (function($) {
        window.fnames = new Array();
        window.ftypes = new Array();
        fnames[0]='EMAIL';
        ftypes[0]='email';
        fnames[1]='MMERGE1';
        ftypes[1]='text';
    }
    (jQuery));
    var $mcj = jQuery.noConflict(true);
    
    function FormReset(){
        document.getElementById('mc-embedded-subscribe-form').reset();
        document.getElementById('news-input').style.border = 'none';
    }
</script>

