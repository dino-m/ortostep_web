<?php
/**
 * @var $this \yii\web\View
 * @var $contact \app\models\FormContact
 */

//use yii\captcha\Captcha;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = Yii::t('app', 'Contact');
?>

<div class="banner-head cont-head">
    <h1>Javite nam se i potražite nas</h1>
</div>

<?php if (!$contact->success && !$contact->hasErrors()): ?>
<section class="contact-fm">
    <h1><span class="heading-border">Javite nam se</span></h1>
    <article>
        <?php $form = ActiveForm::begin(['id'=>'contactform', 'options' => ['type' => 'post', 'name'=>'contactform'], 'action' => ['/index']]); ?>
            <div class="contact-info-group">
                <?= $form->field($contact, 'name')->textInput(['placeholder'=>'Ime i prezime']) ?>
                <?= $form->field($contact, 'email')->textInput(['placeholder'=>'Email']) ?>
                <?= $form->field($contact, 'phone')->textInput(['placeholder'=>'Telefon']) ?>
                <?= $form->field($contact, 'message')->textarea(['rows' => 5, 'placeholder'=>'Poruka']) ?>
            </div>
            <p>Molimo Vas da prije slanja poruke popunite sva polja</p>
            <button class="sendbutton" type="submit"><?= Yii::t('app', 'Pošalji') ?></button>
        <?php ActiveForm::end() ?> 
    </article>
</section>

<?php elseif(!$contact->success && $contact->hasErrors()): ?>
<section class="contact-fm contact-fm-err">
    <h1><span class="heading-border">Javite nam se</span></h1>
    <article>
        <?php $form = ActiveForm::begin(['id'=>'contactform', 'options' => ['type' => 'post', 'name'=>'contactform'], 'action' => ['/index']]); ?>
            <div class="contact-info-group">
                <?= $form->field($contact, 'name')->textInput(['placeholder'=>'Ime i prezime']) ?>
                <?= $form->field($contact, 'email')->textInput(['placeholder'=>'Email']) ?>
                <?= $form->field($contact, 'phone')->textInput(['placeholder'=>'Telefon']) ?>
                <?= $form->field($contact, 'message')->textarea(['rows' => 5, 'placeholder'=>'Poruka']) ?>
            </div>
            <p>Molimo Vas da prije slanja poruke popunite sva polja</p>
            <button class="sendbutton" type="submit"><?= Yii::t('app', 'Pošalji') ?></button>
        <?php ActiveForm::end() ?> 
    </article>
</section>

<?php else: ?>
    <?php if (Yii::$app->language === 'hr'): ?>
    <section class="contact-msg">
            <h1>Hvala Vam!</h1>
            <p>Vaša poruka je uspješno poslana, kao i kopija na Vaš email.</p>
            <p>Kontaktirati ćemo Vas uskoro.</p>
        <?php elseif (Yii::$app->language === 'en'): ?>
            <h1>Thank you!</h1>
            <p>Your message has been successfully sent, along with a copy to email you entered.</p>
            <p>We will contact you back soon.</p>
        <?php endif; ?>
    </section>
<?php endif ?>

<section class="cont-info-main">
    <section class="cont-info-frame">
        <div class="cont-info-row1">
            <div class="cont-map">
                <iframe src="https://www.google.com/maps/d/embed?mid=1dTvfvShpeuGk700rQfG8C-aJhSpMAt-o"></iframe>
            </div>
            <div class="cont-info">
                <div class="cont-info-center">
                    <div class="cont-info-title">
                        <h1><span class="heading-border">Sjedište tvrtke</span></h1>
                    </div>
                    <div class="cont-info-detail">
                        <table>
                            <tr>
                                <td class="cont-td"><i class="fas fa-home" style="font-size:26px;"></i></td> <td><p>Ul. Sv. Leopolda Bogdana Mandića 111K, 31000 Osijek</p></td>
                            </tr>
                            <tr>
                                <td class="cont-td"><i class="fas fa-phone" style="font-size:26px;"></i></td> <td><p>+385 31 / 297 - 393</p></td>
                            </tr>
                            <tr>
                                <td class="cont-mob"><i class="fas fa-mobile" style="font-size:30px;"></i></td> <td><p>+385 91 / 297 - 5335</p></td>
                            </tr>
                            <tr>
                                <td class="cont-td"><i class="fas fa-fax" style="font-size:26px;"></i></td> <td><p>+385 31 / 298 - 382</p></td>
                            </tr>
                            <tr>
                                <td class="cont-td"><i class="fas fa-envelope" style="font-size:26px;"></i></td> <td><p>info@ortostep.hr</p></td>
                            </tr>
                            <tr>
                                <td class="cont-oib">OIB:</td><td><p>&nbsp;72312882449</p></td>
                            </tr>
                        </table>
                        <div class="cont-info-wt"><hr size="2" noshade color="#fff"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cont-info-row2">
            <div class="cont-map">
                <iframe src="https://www.google.com/maps/d/embed?mid=1STMvGAznp4lImgv5mSuAcJebSHRkZcEs"></iframe>
            </div>
            <div class="cont-info">
                <div class="cont-info-center">
                    <div class="cont-info-title">
                        <h1><span class="heading-border">TRGOVINA (Osijek)</span></h1>
                    </div>
                    <div class="cont-info-detail">
                        <table>
                            <tr>
                                <td class="cont-td"><i class="fas fa-home" style="font-size:26px;"></i></td> <td><p>Županijska 20, 31000 Osijek</p></td>
                            </tr>
                            <tr>
                                <td class="cont-mob"><i class="fas fa-mobile" style="font-size:30px;"></i></td> <td><p>+385 91 / 603 - 3365</p></td>
                            </tr>
                            <tr>
                                <td class="cont-td"><i class="fas fa-phone" style="font-size:26px;"></i></td> <td><p>+385 31 / 550 - 305</p></td>
                            </tr>
                            <tr>
                                <td class="cont-td"><i class="fas fa-envelope" style="font-size:26px;"></i></td> <td><p>p1@ortostep.hr</p></td>
                            </tr>
                        </table>
                        <div class="cont-info-wt">
                            <hr size="2" noshade color="#fff">
                            <p>Pon - Pet: 08:00 - 12:00</p>
                            <p>16:00 - 19:00</p>
                            <p>Sub: 08:00 - 13:00</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cont-info-row3">
            <div class="cont-map">
                <iframe src="https://www.google.com/maps/d/embed?mid=1kpKkUuKpnf6IDL-rewh0nLToUYXvDBDi"></iframe>
            </div>
            <div class="cont-info">
                <div class="cont-info-center">
                    <div class="cont-info-title">
                        <h1><span class="heading-border">TRGOVINA (Zagreb)</span></h1>
                    </div>
                    <div class="cont-info-detail">
                        <table>
                            <tr>
                                <td class="cont-td"><i class="fas fa-home" style="font-size:26px;"></i></td> <td><p>Nemčićeva 6, 10000 Zagreb</p></td>
                            </tr>
                            <tr>
                                <td class="cont-mob"><i class="fas fa-mobile" style="font-size:30px;"></i></td> <td><p>+385 91 / 150 - 3715</p></td>
                            </tr>
                            <tr>
                                <td class="cont-td"><i class="fas fa-phone" style="font-size:26px;"></i></td> <td><p>+385 01 / 230 - 0874</p></td>
                            </tr>
                            <tr>
                                <td class="cont-td"><i class="fas fa-envelope" style="font-size:26px;"></i></td> <td><p>p2@ortostep.hr</p></td>
                            </tr>
                        </table>
                        <div class="cont-info-wt">
                            <hr size="2" noshade color="#fff">
                            <p>Pon - Pet: 08:00 - 19:00</p>
                            <p>Sub: 08:00 - 13:00</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cont-info-row4">
            <div class="cont-map">
                <iframe src="https://www.google.com/maps/d/embed?mid=1-zIdVvK_uXd1Pr898r0JKTY9mTQzXqge"></iframe>
            </div>
            <div class="cont-info">
                <div class="cont-info-center">
                    <div class="cont-info-title">
                        <h1><span class="heading-border">TRGOVINA (Zagreb)</span></h1>
                    </div>
                    <div class="cont-info-detail">
                        <table>
                            <tr>
                                <td class="cont-td"><i class="fas fa-home" style="font-size:26px;"></i></td> <td><p>Shopping centar Prečko (2. KAT), 10000 Zagreb</p></td>
                            </tr>
                            <tr>
                                <td class="cont-mob"><i class="fas fa-mobile" style="font-size:30px;"></i></td> <td><p>+385 91 / 297 - 5337</p></td>
                            </tr>
                            <tr>
                                <td class="cont-td"><i class="fas fa-phone" style="font-size:26px;"></i></td> <td><p>+385 01 / 388 - 7990</p></td>
                            </tr>
                            <tr>
                                <td class="cont-td"><i class="fas fa-envelope" style="font-size:26px;"></i></td> <td><p>p4ortostep@gmail.com</p></td>
                            </tr>
                        </table>
                        <div class="cont-info-wt">
                            <hr size="2" noshade color="#fff">
                            <p>Pon - Pet: 09:00 - 16:00</p>
                            <p>Sub: 09:00 - 14:00</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>

<section class="newsletter cont-news">
    <h1><span class="heading-border">NEWSLETTER</span></h1>
    <article>
        <h3>Saznaj na vrijeme za ono najbolje!</h3>
        <div id="mc_embed_signup">
            <form action="https://ortostep.us17.list-manage.com/subscribe/post?u=42874850678ec0fd74567df20&amp;id=2a776a412f" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <div id="mc_embed_signup_scroll">
                    <input type="email" value="" name="EMAIL" class="required email" id="news-input" placeholder="Email">
                    <button type="submit" name="subscribe" id="mc-embedded-subscribe" class="button news-button" onclick="FormReset()">Prijavi se!</button>
                </div>
            </form>
        </div>
    </article>
</section>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
<script type='text/javascript'>
    (function($) {
        window.fnames = new Array();
        window.ftypes = new Array();
        fnames[0]='EMAIL';
        ftypes[0]='email';
        fnames[1]='MMERGE1';
        ftypes[1]='text';
    }
    (jQuery));
    var $mcj = jQuery.noConflict(true);
    
    function FormReset(){
        document.getElementById('mc-embedded-subscribe-form').reset();
        document.getElementById('news-input').style.border = 'none';
    }
</script>
