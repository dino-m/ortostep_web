<?php
/**
 * @var $this \yii\web\View
 */

use yii\helpers\Url;
use yii\helpers\Html;

?>

<?= Html::img(Yii::getAlias('@urlRootImage').'/'.'logo.png'); ?>
<section style="width: 100%; display: inline-block;">
<p style="margin-top: 20px; float: left; width: 300px;">
   Ul. Sv. Leopolda Bogdana Mandića 111k<br>
   31000 Osijek<br>
   Tel: +385 31/297-393<br>
   Mob: +385 91/297-5335<br>
   Fax: +385 31/298-382<br>
   Email: info@ortostep.hr<br>
   OIB: 72312882449
</p>
<p style="text-align: right;">
    Vrijeme zaprimanja: <?php echo date("d.m.Y") . " u " . date("H:i") ?><br>
    Naručitelj: <?php echo $contact['name'] ?> <br>
    E-mail: <?php echo $contact['email'] ?><br>
    Telefon / mobitel: <?php echo $contact['phone']; ?><br>
</p>
</section>

<h1 style="text-align: center;">Narudžba</h1>

<table style="margin-top: 20px; border: 1px solid #000; width: 400px; border-collapse: collapse;">
    <tr>
        <th style="border: 1px solid #000;">R.br.</th>
        <th style="border: 1px solid #000;">Kolekcija</th>
        <th style="border: 1px solid #000;">SKU</th>
        <th style="border: 1px solid #000;">Veličina</th>
        <th style="border: 1px solid #000;">Količina</th>
    </tr>

    <?php

    $i = 0;
    foreach ($cart as $item)
    {
        $i = $i + 1;
        $itemArray = explode(";", $item);
		
        echo "<tr><td style='border: 1px solid #000;'>".$i."</td>";
        echo "<td style='border: 1px solid #000;'>".$itemArray[5]."</td>";
        echo "<td style='border: 1px solid #000;'>".$itemArray[0]."</td>";
        echo "<td style='border: 1px solid #000;'>".$itemArray[1]."</td>";
        echo "<td style='border: 1px solid #000;'>".$itemArray[2]."</td></tr>";
    }
	
    ?>

</table>

<table style="margin-top: 30px;">

    
    <?php

    $i = 0;
    foreach ($cart as $item)
    {
        $i = $i + 1;
        $itemArray = explode(";", $item);
		
        // slika i opis proizvoda
        echo "<tr><th colspan=2><h3>".$itemArray[5]." ".$itemArray[0]."</h3></th></tr>";
        echo "<tr><td colspan=2><img src=http://novi.ortostep.hr/img/products/".$itemArray[3]."></td>";
        echo "<td colspan=3>".$itemArray[4]."</td></tr>";
    }
    ?>

</table>
