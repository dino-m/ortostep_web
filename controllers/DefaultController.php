<?php

namespace app\controllers;

use app\models\Categories;
use app\models\Sizes;
use Yii;
use app\components\web\BaseController;
use app\models\FormContact;
use app\models\Products;
use yii\web\NotFoundHttpException;
use yii\data\Pagination;
use kartik\mpdf\Pdf;

/**
 * @inheritdoc
 */
class DefaultController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => ['class' => 'yii\web\ErrorAction'],
            'captcha' => ['class' => 'yii\captcha\CaptchaAction'],
        ];
    }

    /**
     * Handle star page
     * @return string
     */
    public function actionIndex()
    {
        $contact = new FormContact();
        if (Yii::$app->request->isPost && $contact->load(Yii::$app->request->post())) {
            if ($contact->validate() && $contact->send()) {}
        }
       if ($contact->hasErrors()) {}
        
       return $this->render('index', ['contact' => $contact]);
    }

    /**
     * Handle single category page
     * @param $slug
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionCategory($slug)
    {
        $category = Categories::findOne(['slug' => $slug]);
        $collections = $category->getProducts()->select(['collection'])->distinct();
        $catAnatomskaMediastep = Categories::findOne(['slug' => "anatomska-medistep"]);
        $catDiastep = Categories::findOne(['slug' => "diastep"]);
        $catRadnaObuca = Categories::findOne(['slug' => "radnaobuca"]);
        $catFashion = Categories::findOne(['slug' => "fashion"]);
        
        $catQuery1 = $catAnatomskaMediastep->getProducts();
        $countAnatomskaMediastep = $catQuery1->count();

        $catQuery2 = $catDiastep->getProducts();
        $countDiastep = $catQuery2->count();
        
        $catQuery3 = $catRadnaObuca->getProducts();
        $countRadnaObuca = $catQuery3->count();
        
        $catQuery4 = $catFashion->getProducts();
        $countFashion = $catQuery4->count();
        
        if (empty($category)) {
            throw new NotFoundHttpException(Yii::t('app', 'Category was not found.'));
        }
            if (isset($_GET["collection"]))
            {
                $collectionFilter = $_GET["collection"];
                $categoryActiveQuery = $category->getProducts()->where('collection=:collection', [':collection' => $collectionFilter]);	
            }
            else
            {
                $categoryActiveQuery = $category->getProducts();	
            }
		
        $count = $categoryActiveQuery->count();
        $pagination = new Pagination([
            'defaultPageSize' => 9,
            'totalCount' => $count
        ]);

        $products = $categoryActiveQuery->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('category', [
            'category' => $category,
            'products' => $products,
            'pagination' => $pagination,
            'countAnatomskaMediastep' => $countAnatomskaMediastep,
            'countDiastep' => $countDiastep,
            'countRadnaObuca' => $countRadnaObuca,
            'countFashion' => $countFashion,
            'collection' => $collections->all()
        ]);
    }

    /**
     * Handle single product page
     * @param $slug
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionProduct($slug)
    {
        $product = Products::findOne(['slug' => $slug]);
        $category = Categories::findOne(['slug' => "anatomska-medistep"]);
        $sizesActiveQuery = $product->getSizes();
        $catSlug = substr($slug, 0, 7);

        switch ($catSlug){
            case "anatoms":
                $category = Categories::findOne(['slug' => "anatomska-medistep"]);
                break;
            case "diastep":
                $category = Categories::findOne(['slug' => "diastep"]);
                break;
            case "radnaob":
                $category = Categories::findOne(['slug' => "radnaobuca"]);
                break;
            case "fashion":
                $category = Categories::findOne(['slug' => "fashion"]);
                break;
        }

        if (empty($product)) {
            throw new NotFoundHttpException(Yii::t('app', 'Product was not found.'));
        }
        return $this->render('product', [
            'product' => $product,
            'category' => $category,
            'sizes' => $sizesActiveQuery->all()
        ]);
    }

    /**
     * Handle about page
     * @return string
     */
    public function actionAbout()
    {
        $contact = new FormContact();
        if (Yii::$app->request->isPost && $contact->load(Yii::$app->request->post())) {
            if ($contact->validate() && $contact->send()) {
                //JUST CONTINUE
            }
        }
        if ($contact->hasErrors()) {}
        
        return $this->render('about', ['contact' => $contact]);
    }

    /**
     * Handle contact form
     * @return string
     */
    public function actionContact()
    {
        $contact = new FormContact();
        if (Yii::$app->request->isPost && $contact->load(Yii::$app->request->post())) {
            if ($contact->validate() && $contact->send()) {
                //JUST CONTINUE
            }
        }
        if ($contact->hasErrors()) {}
        
        return $this->render('contact', ['contact' => $contact]);
    }
    
    public function actionHowToShop()
    {
        $contact = new FormContact();
        if (Yii::$app->request->isPost && $contact->load(Yii::$app->request->post())) {
            if ($contact->validate() && $contact->send()) {
                //JUST CONTINUE
            }
        }
        if ($contact->hasErrors()) {}
        
        return $this->render('how-to-shop',['contact' => $contact]);
    }
    
    public function actionCart()
    {
        if (session_status() == PHP_SESSION_NONE)
        {
            session_start();
        }
		
        if ($_SERVER['REQUEST_METHOD'] === 'POST')
        {
            if (!isset($_SESSION['cart']) or isset($_POST['emptyCart']))
            {
                $_SESSION['cart'] = array();
            }
            if (isset($_POST['sendOrder']))
            {
                if (!empty($_POST['email']))
                {
                    $filename = "Narudžba " . date("Y-m-d") . " " . date("H:i") . " " . $_POST['name'] . ".pdf";
                    $sentToVisitor = false;

                    $pdf = new Pdf([
                        'mode' => Pdf::MODE_UTF8,
                        'filename' => $filename,
                        'format' => Pdf::FORMAT_A4,
                        'orientation' => Pdf::ORIENT_PORTRAIT,
                        'destination' => Pdf::DEST_STRING,
                        // your html content input
                        'content' => $this->renderPartial('attachment', ['cart' => $_SESSION['cart'], 'contact' => $_POST]),
                        // format content from your own css file if needed or use the
                        // enhanced bootstrap css built by Krajee for mPDF formatting 
                        //'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
                        // any css to be embedded if required
                        //'cssInline' => '.kv-heading-1{font-size:18px}',
                        'options' => ['title' => 'Vaša narudžba na ortostep.hr'],
                    ]);				

                    // send to visitor
                    $mailer = Yii::$app->mailer->compose('order', ['cart' => $_SESSION['cart'], 'contact' => $_POST]);
                    $mailer->attachContent($pdf->render(), ['fileName' => $filename, 'contentType' => 'application/pdf']);
                    $mailer->setSubject("Potvrda o primitku narudžbe - ortostep.hr");//promijeniti subject i za engl
                    $mailer->setFrom("no-reply@ortostep.hr");
                    $mailer->setReplyTo("no-reply@ortostep.hr");
                    $mailer->setTo($_POST['email']);

                    if ($mailer->send()) {
                        $sentToVisitor = true;
                    }

                    // send to admin
                    $mailer->setSubject("Zaprimljena narudžba - ortostep.hr");//promijeniti subject i za engl
                    $mailer->setFrom($_POST['email']);
                    $mailer->setReplyTo($_POST['email']);
                    $mailer->setTo("dino.mrkonjic@gmail.com");//promijeniti u mail admina*/

                    if ($mailer->send() and $sentToVisitor) {
                        $_SESSION['sendOrderSuccess'] = true;
                        $_SESSION['cart'] = array();
                        unset($_SESSION['sendOrderValidationError']);
                    }
                }
                else
                {
                    $_SESSION['sendOrderValidationError'] = true;
                }
            }

            if (isset($_POST['updateItemQty']))
            {
                $i = 0;
                $updateIdx = $_POST['updateItemQty'];
                $newCart = array();

                foreach ($_SESSION['cart'] as $item)
                {
                    $itemArray = explode(";", $item);

                    if ($i == $updateIdx)
                    {
                        if ($_POST["newQty"] > 0)
                        {
                            $itemArray[2] = $_POST["newQty"];
                        }

                        if ($_POST["newQty"] == 0)
                        {
                            $i = $i + 1;
                            continue;
                        }
                    }

                    array_push($newCart, implode(";", $itemArray));
                    $i = $i + 1;
                }

                $_SESSION['cart'] = $newCart;
            }

            if (isset($_POST['removeItem']))
            {
                $i = 0;
                $removeIdx = $_POST['removeItem'];
                $newCart = array();

                foreach ($_SESSION['cart'] as $item)
                {
                    if ($i != $removeIdx)
                    {
                        array_push($newCart, $item);
                    }

                    $i = $i + 1;
                }

                $_SESSION['cart'] = $newCart;
            }
				
            if (isset($_POST['addItem']))
            {
                if (array_shift($_POST) == "yes")
                {
                    $updated = false;
                    $newCart = array();

                    foreach ($_SESSION['cart'] as $item)
                    {
                        $itemArray = explode(";", $item);

                        if ($_POST["product-sku"] == $itemArray[0] and $_POST["size"] == $itemArray[1])
                        {
                            $itemArray[2] = $itemArray[2] + $_POST["qty"];
                            $updated = true;
                        }

                        array_push($newCart, implode(";", $itemArray));
                    }

                    if (!$updated) array_push($newCart, implode(";", $_POST));

                    $_SESSION['cart'] = $newCart;
                }
            }
        }

        return $this->render('cart');
    }
}
