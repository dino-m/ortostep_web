**INSTALLATION/SETUP**

*COMPOSER*
```
wget https://getcomposer.org/composer.phar
php composer.phar clear-cache
php composer.phar self-update
php composer.phar global require "fxp/composer-asset-plugin:*"
php composer.phar install
```

*DATABASE SETUP*

On existing MySQL instance, create a database called "ortostep" and add a user with all privileges.
After that, modify [./config/bootstrap.php](./config/bootstrap.php) and set the right user and password.
Then run:
```
php yii database/reset
```

*MIGRATION*
```
php yii migrate/up      #MIGRATE UP
php yii migrate/down    #MIGRATE DOWN (THIS DROPS ALL TABLES)
php yii migrate/history #SEE HISTORY
```

*SAMPLES*
```
php yii hydrate/products
php yii hydrate/categories
php yii hydrate/hydrate/put-products-in-categories
```

*RUNNING SERVER*
```
php -S localhost:8080 -t /var/www/dmrkonjic/web
```
