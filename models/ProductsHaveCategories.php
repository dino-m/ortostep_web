<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * @property integer $productId
 * @property integer $categoryId
 * @property integer $order
 *
 * @property Categories $category
 * @property Products $product
 */
class ProductsHaveCategories extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'productsHaveCategories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['productId', 'categoryId'], 'required'],
            [['productId', 'categoryId', 'order'], 'integer'],
            [['categoryId'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['categoryId' => 'id']],
            [['productId'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['productId' => 'id']],
            [['productId', 'categoryId'], 'unique', 'targetAttribute' => ['productId', 'categoryId']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'productId' => Yii::t('app', 'Product ID'),
            'categoryId' => Yii::t('app', 'Category ID'),
            'order' => Yii::t('app', 'Order'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Categories::className(), ['id' => 'categoryId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'productId']);
    }
}
