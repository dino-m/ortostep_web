<?php

namespace app\models;

use Yii;
use yii\base\Model;

class FormContact extends Model
{
    /**
     * @var string
     */
    public $name;
    /**
     * @var string
     */
    public $email;
    /**
     * @var string
     */
    public $phone;
    /**
     * @var string
     */
    public $message;
    /**
     * @var string
     */
    public $captcha;
    /**
     * @var bool
     */
    public $success = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'phone', 'message'], 'required', 'message'=>'Polje ne smije biti prazno!'],
            ['message', 'string'],
            ['email', 'email', 'message'=>'Email nije ispravnog formata!'],
            //['captcha', 'captcha', 'captchaAction' => 'default/captcha'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'message' => Yii::t('app', 'Message'),
            //'captcha' => Yii::t('app', 'Captcha'),
            'success' => Yii::t('app', 'Success'),
        ];
    }

    /**
     * @return bool
     */
    public function send()
    {
        $mailer = Yii::$app->mailer->compose('contact', ['contact' => $this]);
        $mailer->setSubject(APP_NAME . ' - ' . Yii::t('app', 'Contact form'));
        $mailer->setFrom($this->email);
        $mailer->setReplyTo($this->email);
        $mailer->setTo('dino.mrkonjic@gmail.com');
        if ($mailer->send()) {
            $this->success = true;
        }
        return $this->success;
    }
}
