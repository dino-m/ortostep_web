<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property string $email
 * @property string $passwordHash
 * @property string $firstName
 * @property string $lastName
 */
class Users extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'passwordHash'], 'required'],
            [['email', 'passwordHash'], 'string', 'max' => 64],
            [['firstName', 'lastName'], 'string', 'max' => 255],
            [['email', 'passwordHash'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'email' => Yii::t('app', 'Email'),
            'passwordHash' => Yii::t('app', 'Password hash'),
            'firstName' => Yii::t('app', 'First name'),
            'lastName' => Yii::t('app', 'Last name'),
        ];
    }
}
