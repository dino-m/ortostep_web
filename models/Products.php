<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property string $sku
 * @property string $slug
 * @property string $description
 * @property double $price
 * @property string $image
 * @property string $collection
 * 
 *
 * @property ProductsHaveCategories[] $productsHaveCategories
 * @property Categories[] $categories
 * @property Sizes[] $sizes
 */

class Products extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sku', 'slug'], 'required'],
            [['description'], 'string'],
            [['collection'], 'string'],
            [['sku', 'slug'], 'string', 'max' => 128],
            [['image'], 'string', 'max' => 255],
            [['sku', 'slug'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'sku' => Yii::t('app', 'Name'),
            'slug' => Yii::t('app', 'Slug'),
            'description' => Yii::t('app', 'Description'),
            'image' => Yii::t('app', 'Image'),
            'collection' => Yii::t('app', 'Collection'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductsHaveCategories()
    {
        return $this->hasMany(ProductsHaveCategories::className(), ['productId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Categories::className(), ['id' => 'categoryId'])->viaTable('productsHaveCategories', ['productId' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSizes()
    {
        return $this->hasMany(Sizes::className(), ['id' => 'size_id'])->viaTable('productsHaveSizes', ['product_id' => 'id']);
    }

}
