<?php

namespace app\commands;

use app\models\Categories;
use app\models\Products;
use app\models\ProductsHaveCategories;
use yii\console\Controller;
use yii\helpers\Inflector;

/**
 * Hydrating database.
 */
class HydrateController extends Controller
{
    /**
     * Fill categories.
     */
    function actionCategories()
    {
        for ($i = 0; $i < 20; $i++) {
            $s = new Categories([
                'name' => "Category $i",
                'slug' => Inflector::slug("Category $i"),
            ]);
            if ($s->validate()) {
                $s->save();
            }
        }
    }

    /**
     * Fill products.
     */
    function actionProducts()
    {
        for ($i = 0; $i < 20; $i++) {
            $p = new Products([
                'name' => "Product $i",
                'slug' => Inflector::slug("Product $i"),
                'price' => 100.00,
            ]);
            if ($p->validate()) {
                $p->save();
            }
        }
    }

    /**
     * Put random products into random categories.
     */
    function actionPutProductsInCategories()
    {
        $categoryIds = Categories::find()->select(['id'])->all();
        $categoryCount = count($categoryIds);
        foreach (Products::find()->all() as $p) {
            $numberOfCategoriesForThisProduct = rand(1, 10);
            for ($i = 0; $i < $numberOfCategoriesForThisProduct; $i++) {
                $pc = new ProductsHaveCategories([
                    'productId' => $p->id,
                    'categoryId' => $categoryIds[rand(0, $categoryCount - 1)]->id,
                    'order' => 0,
                ]);
                if ($pc->validate()) {
                    $pc->save();
                }
            }
        }
    }
}
