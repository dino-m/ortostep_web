<?php

namespace app\commands;

use yii\console\Controller;
use yii\helpers\Console;
use Yii;

/**
 * Resetting database.
 */
class DatabaseController extends Controller
{
    /**
     * Drop whole database and create it again.
     */
    public function actionReset()
    {
        if (file_exists(APP_INSTALLATION_FILE)) {
            $this->stdout('Delete ' . APP_INSTALLATION_FILE . ' file to force a fresh install.' . PHP_EOL);
            return static::EXIT_CODE_ERROR;
        }
        if (!$this->confirm('Are you sure you want to do this? THIS WILL REMOVE ALL EXISTING DATA (IF ANY) AND RESET DB COMPLETELY!')) {
            return static::EXIT_CODE_ERROR;
        }
        $pdo = new \PDO('mysql:host=' . APP_DATABASE_HOST, APP_DATABASE_USERNAME, APP_DATABASE_PASSWORD);
        $pdo->exec('DROP DATABASE IF EXISTS ' . APP_DATABASE_SCHEMA . '; CREATE DATABASE ' . APP_DATABASE_SCHEMA . ' CHARACTER SET ' . APP_DATABASE_CHARACTER_SET . ' COLLATE ' . APP_DATABASE_COLLATE . ';');
        if ($pdo->errorCode() != '00000') {
            $this->stderr('There were problems while trying to reset database.' . PHP_EOL, Console::FG_RED);
            return static::EXIT_CODE_ERROR;
        } else {
            $this->stdout('Database was successfully DROPPED and CREATED again.' . PHP_EOL, Console::FG_GREEN);
        }
        file_put_contents(APP_INSTALLATION_FILE, Yii::$app->formatter->asDatetime('NOW'));
        return static::EXIT_CODE_NORMAL;
    }
}
